#!/bin/bash

cd $APP_DIR;

if [ ! -z "$ENTRY_MYSQLDUMP" ] ; then
    EXTRA_OPTS="";
    if [ ! -z "$MYSQL_DATABASE" ] ; then
        EXTRA_OPTS="$EXTRA_OPTS $MYSQL_DATABASE";
    fi;

    mysqldump -h $MYSQL_HOST -u $MYSQL_USER --no-create-db --routines --default-character-set=utf8mb4 --tz-utc $EXTRA_OPTS
    exit;
fi;

EXTRA_OPTS="";
if [ ! -z "$MYSQL_DATABASE" ] ; then
    EXTRA_OPTS="$EXTRA_OPTS -D $MYSQL_DATABASE";
fi;

mysql -h $MYSQL_HOST -u $MYSQL_USER $EXTRA_OPTS $@