FROM php:7.4-apache as base

RUN apt-get update -y \
 && apt-get install -y \
    unzip wget \
    libfreetype6-dev libjpeg62-turbo-dev libjpeg-dev libpng-dev libmemcached-dev \
    zlib1g-dev libicu-dev libpq-dev libtidy-dev libzip-dev \
    libldap-dev libgmp-dev \
    # for the image magick extension (imagick)
    libmagickwand-dev \
    # extra dev tools
    git httpie \
 && rm -rf /var/lib/apt/lists/*

RUN docker-php-source extract \
 && docker-php-ext-configure gd --with-freetype --with-jpeg \
 && docker-php-ext-install gd \
 && docker-php-ext-install intl \
 && docker-php-ext-install zip \
 && docker-php-ext-install ldap \
 && docker-php-ext-install gmp \
 && docker-php-ext-install mysqli \
 && docker-php-ext-install pgsql \
 && docker-php-ext-install pdo \
 && docker-php-ext-install pdo_mysql \
 && docker-php-ext-install pdo_pgsql \
 && docker-php-ext-install tidy \
 && docker-php-ext-install exif \
 && docker-php-ext-install bcmath \
 && docker-php-ext-install bz2 \
 && yes '' | pecl install memcached && docker-php-ext-enable memcached \
 && yes '' | pecl install redis && docker-php-ext-enable redis \
 && yes '' | pecl install imagick && docker-php-ext-enable imagick \
 && yes '' | pecl install apcu && docker-php-ext-enable apcu \
 && docker-php-source delete

ARG COMPOSER_VERSION=${COMPOSER_VERSION}

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
 && php -r "if (hash_file('sha384', 'composer-setup.php') === '$(wget -q -O - https://composer.github.io/installer.sig)') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
 && php composer-setup.php --install-dir=/bin --filename=composer \
 && php -r "unlink('composer-setup.php');" \
 && if [ $COMPOSER_VERSION ] ; then composer self-update --$COMPOSER_VERSION ; fi

FROM base as zoey

ARG UID=${UID}
ARG GID=${GID}

ENV APACHE_RUN_USER zoey
ENV APACHE_RUN_GROUP zoey
ENV APACHE_DOCUMENT_ROOT /app/public

RUN groupadd -f -g $GID zoey \
 && useradd -g $GID -d /home/zoey -m -u $UID zoey \
 && a2enmod vhost_alias rewrite ssl



FROM zoey as debug-base

RUN docker-php-source extract \
 && yes '' | pecl install xdebug && docker-php-ext-enable xdebug \
 && docker-php-source delete



FROM zoey as apache



FROM zoey as cli
USER zoey
RUN mkdir /home/zoey/.composer



FROM debug-base as debug-apache



FROM debug-base as debug-cli
USER zoey
