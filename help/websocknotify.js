var hotreloader = new WebSocket("ws://localhost:8001");

hotreloader.addEventListener('open', function () { console.log('Connected!') });
hotreloader.addEventListener('close', function () { console.log('Closed!') });
hotreloader.addEventListener('error', function () { console.log('Errored!') });
hotreloader.addEventListener('message', function () { console.log('Reloading'); window.location.reload(false); });

window.addEventListener('beforeunload', function () { console.log('Closing...'); hotreloader.close(); })