#!/bin/bash

# Portable versions of the path functions (should work on Linux, MacOS and Windows)
function _dirname() { python -c 'import sys; from os.path import realpath, dirname; print(dirname(realpath(sys.argv[-1])))' $@ ; }
function _realpath() { python -c 'import sys; from os.path import realpath; print(realpath(sys.argv[-1]))' $@ ; }
function _hash() { python -c 'import sys,hashlib; print(hashlib.sha1(sys.argv[-1].encode("utf8")).hexdigest()[0:4])' $@ ; }
function _dotenv() { python3 -c 'import sys; from io import StringIO; from pathlib import Path as p; from configparser import ConfigParser as cp; f=sys.argv[-2]; sys.exit() if not p(f).exists() else ""; c=StringIO(); c.write("[default]\n"); c.write(p(f).read_text()); c.seek(0); cp=cp(); cp.read_file(c); v=cp["default"].get(sys.argv[-1]); print(v.strip(chr(39)+chr(34)) if v else "");' $@ ; }

export TGT_DIR=$(_realpath $(pwd))  # the target folder where we run this script from

export USER_ID=$(id -u)
export GROUP_ID=$(id -g)

export CUR_FILE=$(_realpath "$0")  # abs path of this script
export BIN_DIR=$(_dirname $CUR_FILE)  # abs path to the docker/run folder
export DOCKER_DIR=$(_dirname $BIN_DIR) # abs path to the docker folder
export APP_DIR="$(_realpath $DOCKER_DIR/$(_dotenv $DOCKER_DIR/.env APP_ROOT_DIR))" # abs path to the application root folder

export SERVED_DIR=$([ -d "$APP_DIR/public" ] && echo "$APP_DIR/public" || echo "$APP_DIR")
export DOCKERY_PROJECT_NAME="$(basename $APP_DIR)_$(_hash $APP_DIR)"

export CACHE_DIR="$DOCKER_DIR/cache" # cache folder

if [ "$DOCKERY_CFG" = "" ] ; then
    export DOCKERY_CFG="compose/$(_dotenv $DOCKER_DIR/.env DEFAULT_COMPOSE_RECIPE)"
fi;

# Identify the host machine global git config
export GIT_CONFIG=$(git config --show-origin --global -l | head -n 1 | awk '{ print substr($1, index($1, ":")+1); }')

cd $DOCKER_DIR ;
for hook in $(ls -1 hooks/*.sh); do
    . "$hook";
done

docker_compose_params()
{
    echo "-e UID=$USER_ID" \
        "-e GID=$GROUP_ID" \
        "-e APP_DIR=$APP_DIR" \
        "-e TGT_DIR=$TGT_DIR" \
        "-e DOCKER_DIR=$DOCKER_DIR" \
        "-e SERVED_DIR=$SERVED_DIR" \
        "-e DOCKERY_PROJECT_NAME=$DOCKERY_PROJECT_NAME" \
        "-e CACHE_DIR=$CACHE_DIR" \
        "-u $USER_ID:$GROUP_ID" \
        "-v $APP_DIR:$APP_DIR" \
        "-v $TGT_DIR:$TGT_DIR" \
        "-w $TGT_DIR"
}

docker_compose_run()
{
    cd $DOCKER_DIR && \
    docker-compose --project-name "$DOCKERY_PROJECT_NAME" --project-directory "$DOCKER_DIR" -f "$DOCKERY_CFG" run --rm $(docker_compose_params) $EXTRA_FLAGS $1 ${@:2}
}

docker_compose_up()
{
    cd $DOCKER_DIR && \
    GID=$GROUP_ID docker-compose --project-name "$DOCKERY_PROJECT_NAME" --project-directory "$DOCKER_DIR" -f "$DOCKERY_CFG" up --abort-on-container-exit $EXTRA_FLAGS $1 ${@:2}
}

docker_compose_daemon()
{
    cd $DOCKER_DIR && \
    GID=$GROUP_ID docker-compose --project-name "$DOCKERY_PROJECT_NAME" --project-directory "$DOCKER_DIR" -f "$DOCKERY_CFG" up -d $EXTRA_FLAGS $1 ${@:2}
}

docker_compose()
{
    cd $DOCKER_DIR && \
    GID=$GROUP_ID docker-compose --project-name "$DOCKERY_PROJECT_NAME" --project-directory "$DOCKER_DIR" -f "$DOCKERY_CFG" $EXTRA_FLAGS $1 ${@:2}
}

docker_compose_pull()
{
    cd $DOCKER_DIR && \
    GID=$GROUP_ID docker-compose --project-name "$DOCKERY_PROJECT_NAME" --project-directory "$DOCKER_DIR" -f "$DOCKERY_CFG" pull $EXTRA_FLAGS $1 ${@:2}
}
