if grep -q 'XDEBUG_HOST=localhost' "$DOCKER_DIR/.env" ; then
    if [ "$(uname)" = "Darwin" ] ; then
        ## Docker for Mac is run within a VM, so container localhost will be the VM
        ## the host machine can be referenced by a special DNS - host.docker.internal
        ## see https://docs.docker.com/docker-for-mac/networking/
        export XDEBUG_HOST='host.docker.internal'
    fi
fi

if [ "$(uname)" = "Darwin" ] ; then
    ## Docker for Mac is run within a VM, so ssh-agent socket path within the VM
    ## is different from the host machine
    ## see https://github.com/docker/for-mac/issues/410
    export SSH_AUTH_SOCK='/run/host-services/ssh-auth.sock'
fi